package com.example.demo;

import java.time.Duration;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class DemoApplication {

	private static final Logger log = LoggerFactory.getLogger(DemoApplication.class);
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	CustomConfig config;
	
	@GetMapping("/")
	String home() {
		try {
			String home = "My config: " + config.getAutodevopsurl();
			System.out.println(home);
			log.info(home);
			return home;
		} catch (Throwable t) {
			log.error("Errore", t);
			throw t;
		}
	}
	
	@GetMapping("/hello")
	String hello() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.TEXT_PLAIN));
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		String hello = "Autodevops: " + restTemplate.exchange(config.getAutodevopsurl() + "/hello", HttpMethod.GET, entity, String.class).getBody();
		return hello;
	}
	
	@GetMapping("/caller")
	String caller() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.TEXT_PLAIN));
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		String home = "Autodevops: " + restTemplate.exchange(config.getAutodevopsurl() + "/", HttpMethod.GET, entity, String.class).getBody();
		return home;
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
	 
	    return builder
	            .setConnectTimeout(Duration.ofMillis(3000))
	            .setReadTimeout(Duration.ofMillis(3000))
	            .build();
	}
}