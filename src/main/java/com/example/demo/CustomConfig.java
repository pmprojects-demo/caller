package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomConfig {
	
	@Value("${autodevops.url}")
	private String autodevopsurl;

	public String getAutodevopsurl() {
		return autodevopsurl;
	}

	public void setAutodevopsurl(String autodevopsurl) {
		this.autodevopsurl = autodevopsurl;
	}

	@Override
	public String toString() {
		return "CustomConfig [autodevopsurl=" + autodevopsurl + "]";
	}
	
}
